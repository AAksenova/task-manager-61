package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.configuration.ServerConfiguration;
import ru.t1.aksenova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @Nullable
    private static IPropertyService propertyService;

    @Nullable
    private static ConfigurableApplicationContext context;

    @BeforeClass
    public static void initData() {
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        propertyService = context.getBean(IPropertyService.class);
    }

    @AfterClass
    public static void destroy() {
        context.close();
    }

    @Test
    @Ignore
    public void getApplicationVersion() {
        Assert.assertNotNull(propertyService.getApplicationVersion());
    }

    @Test
    @Ignore
    public void getAuthorName() {
        Assert.assertNotNull(propertyService.getAuthorName());
    }

    @Test
    public void getAuthorEmail() {
        Assert.assertNotNull(propertyService.getAuthorEmail());
    }

    @Test
    public void getGitBranch() {
        Assert.assertNotNull(propertyService.getGitBranch());
    }

    @Test
    public void getGitCommitId() {
        Assert.assertNotNull(propertyService.getGitCommitId());
    }

    @Test
    public void getGitCommitTime() {
        Assert.assertNotNull(propertyService.getGitCommitTime());
    }

    @Test
    public void getGitCommitMessage() {
        Assert.assertNotNull(propertyService.getGitCommitMessage());
    }

    @Test
    public void getGitCommitterName() {
        Assert.assertNotNull(propertyService.getGitCommitterName());
    }

    @Test
    public void getGitCommitterEmail() {
        Assert.assertNotNull(propertyService.getGitCommitterEmail());
    }

    @Test
    public void getPasswordIteration() {
        Assert.assertNotNull(propertyService.getPasswordIteration());
    }

    @Test
    public void getPasswordSecret() {
        Assert.assertNotNull(propertyService.getPasswordSecret());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(propertyService.getSessionTimeout());
    }

}
