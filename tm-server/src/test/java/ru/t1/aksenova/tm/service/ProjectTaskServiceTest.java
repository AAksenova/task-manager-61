package ru.t1.aksenova.tm.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.api.service.dto.IUserDTOService;
import ru.t1.aksenova.tm.configuration.ServerConfiguration;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.migration.AbstractSchemeTest;

import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
@Ignore
public final class ProjectTaskServiceTest extends AbstractSchemeTest {

    @Nullable
    private static IProjectDTOService projectService;

    @Nullable
    private static ITaskDTOService taskService;

    @Nullable
    private static IUserDTOService userService;

    @Nullable
    private static ConfigurableApplicationContext context;

    @Nullable
    private static IProjectTaskDTOService projectTaskService;

    @NotNull
    private static String userId = "";

    public static void initConnectionService() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    public static void clearData() {
        @Nullable UserDTO user = userService.findOneById(userId);
        if (user != null) userService.remove(user);
    }

    @BeforeClass
    public static void initData() throws Exception {
        initConnectionService();
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        projectService = context.getBean(IProjectDTOService.class);
        taskService = context.getBean(ITaskDTOService.class);
        projectTaskService = context.getBean(IProjectTaskDTOService.class);
        userService = context.getBean(IUserDTOService.class);

        @NotNull final UserDTO user = userService.add(USER_TEST);
        userId = user.getId();
        USER_PROJECT1.setUserId(userId);
        USER_PROJECT2.setUserId(userId);
        USER_TASK1.setUserId(userId);
        USER_TASK2.setUserId(userId);
        USER_TASK3.setUserId(userId);
    }

    @AfterClass
    public static void destroy() {
        clearData();
        context.close();
    }

    @Before
    public void beforeTest() {
        projectService.add(USER_PROJECT1);
        projectService.add(USER_PROJECT2);
        taskService.add(USER_TASK1);
        taskService.add(USER_TASK2);
        taskService.add(USER_TASK3);
    }

    @After
    public void afterTest() {
        taskService.removeAll(userId);
        projectService.removeAll(userId);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final TaskDTO task = taskService.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final TaskDTO task = taskService.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(null, USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject("", USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, NON_EXISTING_PROJECT_ID));

        @NotNull List<TaskDTO> tasks = taskService.findAllByProjectId(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(tasks);
        projectTaskService.removeTaskToProject(userId, USER_PROJECT1.getId());
    }

}
