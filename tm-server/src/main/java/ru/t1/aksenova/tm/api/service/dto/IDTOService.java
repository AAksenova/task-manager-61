package ru.t1.aksenova.tm.api.service.dto;

import ru.t1.aksenova.tm.dto.model.AbstractModelDTO;

public interface IDTOService<M extends AbstractModelDTO> {

    M add(M model);

    void update(M model);

    void remove(M model);

}
