package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> {

    List<M> findByUserId(@NotNull final String userId);

    Optional<M> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);
    
    long countByUserId(@NotNull final String userId);

}
