package ru.t1.aksenova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.ProjectSort;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable Project project);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable Project project
    );

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull Collection<Project> set(@NotNull Collection<Project> projects);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @Nullable
    Project findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    Project removeOne(
            @Nullable String userId,
            @Nullable Project project
    );

    @Nullable
    Project removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);
}
