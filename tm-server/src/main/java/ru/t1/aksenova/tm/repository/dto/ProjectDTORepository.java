package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> {

    List<ProjectDTO> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @Query("SELECT COUNT(m) = 1 FROM ProjectDTO m WHERE m.id = :id AND m.userId = :userId")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

}
