package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IDTOService<M> {

    M add(@NotNull String userId, M model);

    void update(@NotNull String userId, M model);

    void remove(@NotNull String userId, M model);

}
