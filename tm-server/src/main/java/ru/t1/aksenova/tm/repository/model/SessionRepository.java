package ru.t1.aksenova.tm.repository.model;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.model.Session;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    @Query("SELECT COUNT(m) = 1 FROM SessionDTO m WHERE m.id = :id AND m.userId = :userId")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    @Query("SELECT COUNT(m) = 1 FROM UserDTO m WHERE m.id = :id")
    boolean existById(@Param("id") String id);

}
