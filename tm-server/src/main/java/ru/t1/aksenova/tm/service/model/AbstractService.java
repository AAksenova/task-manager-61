package ru.t1.aksenova.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.model.IService;
import ru.t1.aksenova.tm.model.AbstractModel;
import ru.t1.aksenova.tm.repository.model.AbstractRepository;

@Service
@NoArgsConstructor
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected abstract AbstractRepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        getRepository().save(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull M model) {
        getRepository().save(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull M model) {
        getRepository().delete(model);
    }

}
