package ru.t1.aksenova.tm.repository.dto;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.dto.model.SessionDTO;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> {

    @Query("SELECT COUNT(m) = 1 FROM SessionDTO m WHERE m.id = :id AND m.userId = :userId")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

}
