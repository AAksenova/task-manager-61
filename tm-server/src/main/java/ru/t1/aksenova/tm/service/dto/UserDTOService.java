package ru.t1.aksenova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.ISessionDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.api.service.dto.IUserDTOService;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.entity.UserNotFoundException;
import ru.t1.aksenova.tm.exception.field.EmailEmptyException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.LoginEmptyException;
import ru.t1.aksenova.tm.exception.field.PasswordEmptyException;
import ru.t1.aksenova.tm.exception.user.ExistEmailException;
import ru.t1.aksenova.tm.exception.user.ExistLoginException;
import ru.t1.aksenova.tm.exception.user.RoleEmptyException;
import ru.t1.aksenova.tm.repository.dto.UserDTORepository;
import ru.t1.aksenova.tm.util.HashUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserDTOService extends AbstractDTOService<UserDTO> implements IUserDTOService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @Nullable
    @Autowired
    private UserDTORepository repository;

    @NotNull
    @Override
    protected UserDTORepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        add(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistEmailException(email);
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException(login);
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public Collection<UserDTO> set(@NotNull Collection<UserDTO> users) {
        if (users.isEmpty()) return Collections.emptyList();
        users.forEach(getRepository()::save);
        return users;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Optional<UserDTO> user = getRepository().findById(id);
        if (!user.isPresent()) throw new TaskNotFoundException();
        return user.orElse(null);

    }

    @NotNull
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = getRepository().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = getRepository().findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Nullable
    @Override
    public UserDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Optional<UserDTO> user = getRepository().findById(id);
        if (user.isPresent()) removeOne(user.orElse(null));
        return user.orElse(null);
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().deleteAll();
    }

    @Nullable
    @Override
    public UserDTO removeOne(@Nullable final UserDTO user) {
        if (user == null) throw new TaskNotFoundException();
        taskService.removeAll(user.getId());
        projectService.removeAll(user.getId());
        sessionService.removeAll(user.getId());
        remove(user);
        return user;
    }

    @Nullable
    @Override
    public UserDTO removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        return removeOne(user);
    }

    @Nullable
    @Override
    public UserDTO removeOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        return removeOne(user);
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        UserDTO user = getRepository().findByLogin(login);
        return (user != null);
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return (getRepository().findByEmail(email) != null);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findByLogin(login);
        user.setLocked(false);
        update(user);
    }

}
