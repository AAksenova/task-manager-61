package ru.t1.aksenova.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.dto.IDTOService;
import ru.t1.aksenova.tm.dto.model.AbstractModelDTO;
import ru.t1.aksenova.tm.repository.dto.AbstractDTORepository;


@Service
@NoArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO>
        implements IDTOService<M> {

    @NotNull
    protected abstract AbstractDTORepository<M> getRepository();

    @Override
    @NotNull
    @Transactional
    public M add(@NotNull final M model) {
        getRepository().save(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull M model) {
        getRepository().save(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull M model) {
        getRepository().delete(model);
    }

}
