package ru.t1.aksenova.tm.api.service.model;

import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> {
    @Transactional
    M add(M model);

    @Transactional
    void update(M model);

    @Transactional
    void remove(M model);
}
