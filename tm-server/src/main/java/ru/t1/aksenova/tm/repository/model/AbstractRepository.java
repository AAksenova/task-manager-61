package ru.t1.aksenova.tm.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.aksenova.tm.model.AbstractModel;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AbstractRepository<M extends AbstractModel> extends JpaRepository<M, String> {

    long count();

    List<M> findAll();

    Optional<M> findById(@Nullable final String id);

    void deleteAll();

    void deleteById(@Nullable final String id);

}
