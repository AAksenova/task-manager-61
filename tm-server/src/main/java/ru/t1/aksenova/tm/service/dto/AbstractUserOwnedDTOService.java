package ru.t1.aksenova.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.repository.dto.AbstractUserOwnedDTORepository;


@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTOService<M> implements IUserOwnedDTOService<M> {

    @Nullable
    protected abstract AbstractUserOwnedDTORepository<M> getRepository();

    @Override
    @Nullable
    @Transactional
    public M add(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return getRepository().save(model);
    }

    @Override
    @Transactional
    public void update(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        if (getRepository().findByUserIdAndId(model.getUserId(), model.getId()) == null) return;
        getRepository().save(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        if (getRepository().findByUserIdAndId(model.getUserId(), model.getId()) == null) return;
        getRepository().delete(model);
    }

}
