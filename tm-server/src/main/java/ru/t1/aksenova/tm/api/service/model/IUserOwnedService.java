package ru.t1.aksenova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> {

    M add(@NotNull String userId, M model);

    void update(@NotNull String userId, M model);

    void remove(@NotNull String userId, M model);

}
