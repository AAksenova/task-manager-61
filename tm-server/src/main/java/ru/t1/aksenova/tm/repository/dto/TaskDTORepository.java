package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> {

    List<TaskDTO> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    List<TaskDTO> findByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    void deleteByProjectId(@NotNull final String projectId);

    @Query("SELECT COUNT(m) = 1 FROM TaskDTO m WHERE m.id = :id AND m.userId = :userId")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

}
