package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.SessionDTO;

import java.util.Date;
import java.util.List;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

    @NotNull
    SessionDTO add(@Nullable SessionDTO session);

    @NotNull
    SessionDTO create(
            @Nullable String userId,
            @Nullable String role
    );

    @NotNull
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    SessionDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    SessionDTO removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    SessionDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String role,
            @Nullable Date date
    );

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);

}
