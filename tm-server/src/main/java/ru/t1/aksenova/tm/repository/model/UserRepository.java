package ru.t1.aksenova.tm.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.aksenova.tm.model.User;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public interface UserRepository extends AbstractRepository<User> {

    User findByLogin(@Nullable final String login);

    User findByEmail(@Nullable final String email);

    void deleteByLogin(@Nullable final String login);

    void deleteByEmail(@Nullable final String email);

    @Query("SELECT COUNT(m) = 1 FROM UserDTO m WHERE m.login = :login")
    boolean isLoginExist(@Param("login") String login);

    @Query("SELECT COUNT(m) = 1 FROM UserDTO m WHERE m.email = :email")
    boolean isEmailExist(@Param("email") String email);

    @Query("SELECT COUNT(m) = 1 FROM UserDTO m WHERE m.id = :id")
    boolean existById(@Param("id") String id);

}
