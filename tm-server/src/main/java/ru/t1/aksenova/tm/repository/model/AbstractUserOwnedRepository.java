package ru.t1.aksenova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.aksenova.tm.model.AbstractUserOwnedModel;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> {

    List<M> findByUserId(@NotNull final String userId);

    Optional<M> findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    long countByUserId(@NotNull final String userId);

    boolean existByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
