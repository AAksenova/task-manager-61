package ru.t1.aksenova.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aksenova.tm.api.service.model.IUserOwnedService;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.model.AbstractUserOwnedModel;

@Service
@AllArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {

    @Nullable
    @Override
    @Transactional
    public M add(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        getRepository().save(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        getRepository().save(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        getRepository().delete(model);
    }

}
