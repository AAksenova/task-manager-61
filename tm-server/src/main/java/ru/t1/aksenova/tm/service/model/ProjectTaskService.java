package ru.t1.aksenova.tm.service.model;


import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aksenova.tm.api.service.model.IProjectService;
import ru.t1.aksenova.tm.api.service.model.IProjectTaskService;
import ru.t1.aksenova.tm.api.service.model.ITaskService;
import ru.t1.aksenova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aksenova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aksenova.tm.exception.field.TaskIdEmptyException;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Task;

import java.util.List;

@Service
@AllArgsConstructor
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private final IProjectService projectService;

    @NotNull
    @Autowired
    private final ITaskService taskService;

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) return;
        task.setProject(project);
        taskService.update(userId, task);
    }

    @Override
    public void unbindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) return;
        task.setProject(null);
        taskService.update(userId, task);
    }

    @Override
    public void removeTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        if (!tasks.isEmpty()) {
            for (final Task task : tasks) taskService.removeOneById(userId, task.getId());
        }
        projectService.removeOneById(userId, projectId);
    }

}
