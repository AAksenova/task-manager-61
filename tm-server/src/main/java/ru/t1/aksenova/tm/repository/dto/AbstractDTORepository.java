package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.aksenova.tm.dto.model.AbstractModelDTO;

import java.util.List;
import java.util.Optional;


@NoRepositoryBean
public interface AbstractDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

    long count();

    List<M> findAll();

    Optional<M> findById(@Nullable final String id);

    void deleteAll();

    void deleteById(@Nullable final String id);

    boolean existsById(@NotNull final String id);

}
